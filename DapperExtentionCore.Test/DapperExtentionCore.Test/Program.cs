﻿using DapperExtensions;
using System;
using System.Data;

namespace DapperExtentionCore.Test
{
    class Program
    {
        private static readonly IDatabase _database = DatabaseFactory.CreateDatabase("Eloan365Con");
        public static DataTable GetLoanExtendTime(out int total)
        {

            //DataTable dt = new DataTable();
            total = 0;
            try
            {
                //var list = new List<VWLoanAwaitExtendTimeLimit>();
                string Sql =
                         @"
                       SELECT top  10  [ID]
                      ,[Userid]
                      ,[Nocode]
                      ,[Jkbt]
                      ,[Jkje]
                      ,[Nll]
                      ,[ms]
                      ,[tbsz]
                      ,[Jkqx]
                      ,[Fbx]
                      ,[Xydj]
                      ,[Nickname]
                      ,[Truename]
                      ,[hkzt]
                      ,[hkfs]
                      ,[shdt]
                      ,[AgentId]
                      ,[AgentName]
                      ,[AgentType]
                      ,[Postion]
                       FROM [dbo].[VW_LoanAwaitExtendTimeLimit]
                       Where 1=1 ";
                DataTable list = new DataTable();
                    
                list = _database.GetDataTableByPage12(Sql, "*", "shdt desc",
                        10, 1, out total);
                
                //_database.
                return list;
            }
            catch (Exception ex)
            {
                throw new Exception("查询！", ex);
            }
        }
        static void Main(string[] args)
        {

            Console.WriteLine("Hello World!");
            Console.WriteLine("测试对应dapper extentions!");

            int totol = 0;
            var dt = GetLoanExtendTime(out totol);
            Console.WriteLine("totol:{0}", totol);

            Console.ReadKey();
        }
    }
}
