﻿using DapperExtensions;
using DapperExtensions.Mapper;
using DapperExtensions.Sql;
using DapperExtentionCore.Test.DapperMapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DapperExtentionCore.Test
{
    public class AppConfig {
        internal static IConfigurationRoot Configuration { get; set; }
        public static IConfigurationSection GetSection(string name)
        {
            return Configuration?.GetSection(name);
        }
    }
    public class DatabaseFactory
    {
        #region Private Fields

        private static readonly ConcurrentDictionary<string, IDatabase> _databases = new ConcurrentDictionary<string, IDatabase>();

        private static readonly object _getlock = new object();
        #endregion

        #region Public Methods

        public static IDatabase CreateDatabase()
        {
            return CreateDatabase("Eloan365Con");
        }

        public static IDatabase CreateDatabase(string connectionName)
        {

            lock (_getlock)
            {
                IDatabase db = null;
                //||
                if (!_databases.ContainsKey(connectionName))
                {
                    string connectionString =
                        //"Data Source=192.168.99.204;Initial Catalog=Eloan365V5;User ID=admin3210;Password=admin3210;Max Pool Size=2500;Connect Timeout=300";
                        //AppConfig.Configuration.GetConnectionString(connectionName);
                        System.Configuration.ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;

                    IDatabase database = new Database(new SqlConnection(connectionString),
                        new SqlGeneratorImpl(new DapperExtensionsConfiguration(typeof(AutoClassMapper<>),
                        new List<Assembly>(), new SqlServerDialect())));

                    _databases.TryAdd(connectionName, database);

                    return database;

                }
                else
                {
                    _databases.TryGetValue(connectionName, out db);
                    return db;
                }
            }


        }

        public static IDatabase CreateDatabase(string connectionName, IDbConnection connection)
        {
            if (_databases.ContainsKey(connectionName))
            {
                return _databases[connectionName];
            }
            string connectionString =
                //"Data Source=192.168.99.204;Initial Catalog=Eloan365V5;User ID=admin3210;Password=admin3210;Max Pool Size=2500;Connect Timeout=300";
                //AppConfig.Configuration.GetConnectionString(connectionName);
                System.Configuration.ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
            if (connection == null)
            {
                connection = new SqlConnection(connectionString);
            }
            IDatabase database = new Database(connection,
                new SqlGeneratorImpl(new DapperExtensionsConfiguration(typeof(AutoClassMapper<>),
                new List<Assembly>(), new SqlServerDialect())));
            _databases.TryAdd(connectionName, database);

            return database;
        }
        #endregion
    }
}
