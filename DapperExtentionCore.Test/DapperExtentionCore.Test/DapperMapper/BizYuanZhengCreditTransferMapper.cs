﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DapperExtentionCore.Test.DapperMapper
{
    public partial class Biz_YuanZhengCreditTransfer
    {
        public int Id { get; set; }
        public int TransferUserId { get; set; }
        public int UnderTakeUserId { get; set; }
        public decimal TransferAmount { get; set; }
        public System.DateTime TransferTime { get; set; }
        public bool IsSendSiteMsg { get; set; }
        public bool IsAgree { get; set; }
        public string LoanIdList { get; set; }
        public string LoanType { get; set; }
        public string HtmlStr { get; set; }
    }
    public class BizYuanZhengCreditTransferMapper : ClassMapper<Biz_YuanZhengCreditTransfer>
    {
        public BizYuanZhengCreditTransferMapper()
        {
            Table("Biz_YuanZhengCreditTransfer");
            base.TableName = "Biz_YuanZhengCreditTransfer";
            Map(x => x.Id).Key(KeyType.Identity);
            AutoMap();
        }
    }
}
